package model;

import java.util.List;

/**
 *
 * @author Fernando
 */
public class Pedido {

    private String molho;
    private String massa;
    private List<String> ingredientes;

    public String getMolho() {
        return molho;
    }

    public void setMolho(String molho) {
        this.molho = molho;
    }

    public String getMassa() {
        return massa;
    }

    public void setMassa(String massa) {
        this.massa = massa;
    }

    public List<String> getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(List<String> ingredientes) {
        this.ingredientes = ingredientes;
    }
        
}
