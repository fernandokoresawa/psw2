import Model.Cliente;
import util.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Rafael.Soares
 */
public class IncluirCliente extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            String nomeCliente =  request.getParameter("nomeCliente");
            String enderecoCliente = request.getParameter("enderecoCliente");
            String cpfCliente = request.getParameter("cpfCliente");
            
            out.println("Incluindo...<br/>");
            out.println("nome:" + nomeCliente + "<br/>");
            out.println("endereco:" + enderecoCliente + "<br/>");
            out.println("cpf:" + cpfCliente + "<br/>");
            
            
            Cliente cliente = new Cliente();
            cliente.setCpfCliente(cpfCliente);
            cliente.setEnderecoCliente(enderecoCliente);
            cliente.setNomeCliente(nomeCliente);
            
            
            //Criar a sessão
            Session sessao = HibernateUtil
                    .getSessionFactory()
                    .openSession();
            
            //Criar a transação
            Transaction t = sessao.beginTransaction();
            
            //Falar que quer salvar
            sessao.save(cliente);
            
            //Mandar salvar
            sessao.flush();
            
            //Commitar a transação
            t.commit();
            
            //Fechar a sessao
            sessao.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
