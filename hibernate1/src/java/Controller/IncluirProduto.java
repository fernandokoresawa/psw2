package Controller;

import Util.HibernateUtil;
import Model.Produto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Fernando
 */
public class IncluirProduto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            String strNomeProduto = request.getParameter("nomeProduto");
            String strDescricaoProduto = request.getParameter("descProduto");
            float flPrecoProduto = Float.parseFloat(request.getParameter("precoProduto"));
            
            
            Produto produto = new Produto();
            produto.setNomeProduto(strNomeProduto);
            produto.setDescricaoProduto(strDescricaoProduto);
            produto.setPrecoProduto(flPrecoProduto);
            
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            
            Transaction transacao = sessao.beginTransaction();
            
            sessao.save(produto);
            
            sessao.flush();
            
            transacao.commit();
            
            sessao.close();
            

            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet IncluirProduto</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Produto incluído -> " + strNomeProduto + "</h1>");
            out.println("</body>");
            out.println("</html>");

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
