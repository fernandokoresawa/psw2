package Model;

import javax.persistence.*;

/**
 *
 * @author fernando
 */

@Entity
public class Produto {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idProduto;
    private String nomeProduto;
    private String descricaoProduto;
    private float precoProduto;
    
    public int getIdProduto(){
        return idProduto;
    }
    
    public void setIdProduto(int idProduto){
        this.idProduto = idProduto;
    }
    
    public String getNomeProduto(){
        return nomeProduto;
    }
    
    public void setNomeProduto(String nomeProduto){
        this.nomeProduto = nomeProduto;
    }
    
    public String getDescricaoProduto(){
        return descricaoProduto;
    }
    
    public void setDescricaoProduto(String descricaoProduto){
        this.descricaoProduto = descricaoProduto;
    }
    
    public float getPrecoProduto(){
        return precoProduto;
    }
    
    public void setPrecoProduto(float precoProduto){
        this.precoProduto = precoProduto;
    }
    
}
